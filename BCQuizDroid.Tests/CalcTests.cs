﻿using NUnit.Framework;
using System;

namespace BCQuizDroid.Tests
{
    class Calc
    {
        public int Add(int l, int r)
        {
            if (l > 100 || r > 100)
                throw new Exception ("Can't hold more than 200");
            return l + r;
        }
        public int Sub(int l, int r)
        {
            if (l > 100 || r > 100)
                throw new Exception ("Can't hold more than 200");
            return l - r;
        }
    }


    [TestFixture ()]
    public class CalcTests
    {
        int l;
        int r;
        Calc calc;

        [SetUp()]
        public void InitializeLAndR()
        {
            l = 2;
            r = 2;
            calc = new Calc ();
        }

        [Test ()]
        public void DoesAdditionWork ()
        {
            // Act
            var answer = calc.Add(l, r);

            // Assert
            Assert.AreEqual (4, answer);
        }
        [Test()]
        public void DoesSubtractionWork()
        {
            var answer = calc.Sub(l, r);

            Assert.AreEqual (0, answer);
        }
        [Test()]
        public void DoesSubtractionWorkWrongly()
        {
            var answer = calc.Sub (l, r);

            Assert.AreNotEqual (-2, answer);
        }
        [Test()]
        [ExpectedException(typeof(System.Exception))]
        public void CanIAdd100To100()
        {
            var bigL = 100;
            var bigR = 100;

            var answer = calc.Add (bigL, bigR);
            
            Assert.Fail ("Shouldn't get here");
        }
    }
}

