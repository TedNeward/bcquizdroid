﻿using System;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Util;

// 

namespace BCQuizDroid
{
    [Activity (Label = "BCQuizDroid", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : ListActivity
    {
        private const string TAG = "MainActivity";

        List<string> topics = new List<string>();

        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            ITopicRepository repo = (this.Application as QuizApp).TopicRepo;
            foreach (var t in repo.GetAllTopics()) {
                topics.Add (t.Title);
            }

            var textListResourceID = Android.Resource.Layout.SimpleListItem1;
            this.ListAdapter = new ArrayAdapter<String> (this, textListResourceID, topics.ToArray());
        }

        protected override void OnListItemClick (ListView l, View v, int position, long id)
        {
            base.OnListItemClick (l, v, position, id);

            //Toast.MakeText (this, "You selected " + topics [position], ToastLength.Short).Show ();
            Log.Info (TAG, "User selected " + position + "(" + topics [position] + ")");

            var intent = new Intent (this, typeof(QuestionActivity));
            intent.PutExtra ("topicPosition", position);
            intent.PutExtra ("topicName", topics [position]);

            StartActivity (intent);
        }
    }
}


