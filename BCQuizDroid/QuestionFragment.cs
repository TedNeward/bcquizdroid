﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace BCQuizDroid
{
    public class QuestionFragment : Fragment
    {
        public interface Listener
        {
            void OnNextQuestion();
        }

        private const string TAG = "QuestionFragment";

        TextView text;
        RadioButton answer1;
        RadioButton answer2;
        RadioButton answer3;
        RadioButton answer4;
        Button next;

        public Question Question {
            get;
            set;
        }

        public override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View v = inflater.Inflate(Resource.Layout.question_fragment, container, false);

            text = v.FindViewById<TextView> (Resource.Id.questionText);
            text.Text = Question.Text;

            answer1 = v.FindViewById<RadioButton> (Resource.Id.answer1);
            answer1.Text = Question.Answer1;

            answer2 = v.FindViewById<RadioButton> (Resource.Id.answer2);
            answer2.Text = Question.Answer2;

            answer3 = v.FindViewById<RadioButton> (Resource.Id.answer3);
            answer3.Text = Question.Answer3;

            answer4 = v.FindViewById<RadioButton> (Resource.Id.answer4);
            answer4.Text = Question.Answer4;

            next = v.FindViewById<Button> (Resource.Id.next);
            next.Click += (object sender, EventArgs args) => {
                Log.Info(TAG, String.Format("Correct = {0}, Selected1 = {1}, Selected2 = {2}, Selected3 = {3}, Selected4 = {4}",
                    Question.Correct,
                    answer1.Checked,
                    answer2.Checked,
                    answer3.Checked,
                    answer4.Checked));

                if ( (answer1.Checked && Question.Correct == 1) ||
                    (answer2.Checked && Question.Correct == 2) ||
                    (answer3.Checked && Question.Correct == 3) ||
                    (answer4.Checked && Question.Correct == 4) )
                {
                    Toast.MakeText(v.Context, "CORRECT!", ToastLength.Short).Show();

                    var owner = this.Activity as Listener;
                    owner.OnNextQuestion();
                }
                else
                {
                    Toast.MakeText(v.Context, "WRONG!", ToastLength.Short).Show();
                }
            };

            return v;
        }
    }
}

