﻿using System;
using Android.App;
using Android.Runtime;
using Android.Util;

namespace BCQuizDroid
{
    [Application]
    public class QuizApp : Application
    {
        private const string TAG = "QuizApp";

        public QuizApp (IntPtr ip, JniHandleOwnership jho)
            : base(ip, jho)
        {
        }

        public override void OnCreate ()
        {
            base.OnCreate ();

            Log.Info (TAG, "OnCreate()");

            topicRepo = new InMemTopicRepository();
        }

        public ITopicRepository TopicRepo { get { return topicRepo; } }
        private ITopicRepository topicRepo;
    }
}

