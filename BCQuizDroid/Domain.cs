﻿using System;
using System.Collections.Generic;

namespace BCQuizDroid
{
    public interface ITopicRepository
    {
        List<Topic> GetAllTopics();
        Topic GetTopicById(int id);
        Topic GetTopicByTitle(string title);

        // bool AddTopic(Topic newTopic);
        // bool UpdateTopic(Topic topic);
        // bool DeleteTopic(Topic topic);
    }

    public class InMemTopicRepository : ITopicRepository
    {
        private List<Topic> topics = new List<Topic>();

        public InMemTopicRepository()
        {
            topics.Add(new Topic("Mathematics", "Can you add?", new List<Question>() {
                new Question ("What is 2+2?", "4", "22", "-4", "No one knows", 1),
                new Question ("What is 3+3?", "6", "33", "4", "No one knows", 1)
            }));

            topics.Add(new Topic("Science", "Can you blow stuff up?", new List<Question>()));

            topics.Add(new Topic("News", "Do you watch CNN?", new List<Question>()));

            topics.Add(new Topic("Marvel Super Heroes", 
                "Avengers, Spider-Man, Fantastic Four, oh my!", new List<Question>()));
        }

        //private static InMemTopicRepository instance = new InMemTopicRepository();
        //public static ITopicRepository Instance { get { return instance; } }

        public List<Topic> GetAllTopics() {
            return topics;
        }
        public Topic GetTopicById(int id) {
            return topics [id];
        }
        public Topic GetTopicByTitle(string title) {
            foreach (var t in topics)
                if (t.Title == title)
                    return t;
            return null;
        }
    }

    public class Topic
    {
        public Topic()
            : this("", "", new List<Question>())
        {
        }
        public Topic(string t, string d, List<Question> l)
        {
            Title = t; Description = d; Questions = l;
        }

        public string Title { get; set; }
        public string Description { get; set; }
        public List<Question> Questions { get; set; }
    }

    public class Question
    {
        public Question()
            : this ("", "", "", "", "", -1)
        { }

        public Question(string t, string a1, string a2, string a3, string a4, int c)
        {
            Text = t;
            Answer1 = a1;
            Answer2 = a2;
            Answer3 = a3;
            Answer4 = a4;
            Correct = c;
        }

        public string Text { get; set; }
        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }
        public int Correct { get; set; }
    }
}

