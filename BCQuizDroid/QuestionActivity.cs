﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

// TODO: 

namespace BCQuizDroid
{
    [Activity (Label = "QuestionActivity")]            
    public class QuestionActivity : Activity, QuestionFragment.Listener
    {
        private TextView textView1;
        private int currentQuestion = 0;
        private Topic thisTopic;

        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            // Set layout content here
            SetContentView (Resource.Layout.question_activity);

            textView1 = FindViewById<TextView> (Resource.Id.textView1);

            var sendingIntent = this.Intent;
            int whichTopic = sendingIntent.Extras.GetInt ("topicPosition");

            thisTopic = (Application as QuizApp).TopicRepo.GetTopicById(whichTopic);
            textView1.Text = thisTopic.Title;

            currentQuestion = 0; //new Random ().Next (thisTopic.Questions.Count);

            // Create the QuestionFragment and put it into the UI
            DisplayQuestion (currentQuestion);
        }

        private void DisplayQuestion(int questionNum) {
            QuestionFragment qf = new QuestionFragment ();
            qf.Question = thisTopic.Questions [questionNum];

            View fragmentPlaceholder = FindViewById<FrameLayout> (Resource.Id.fragmentPlaceholder);

            FragmentTransaction fragmentTx = this.FragmentManager.BeginTransaction();
            fragmentTx.Replace (Resource.Id.fragmentPlaceholder, qf);
            //fragmentTx.Add(Resource.Id.fragmentPlaceholder, qf);
            fragmentTx.Commit ();
        }

        public void OnNextQuestion() {
            currentQuestion++;

            if (currentQuestion < thisTopic.Questions.Count) {
                DisplayQuestion (currentQuestion);
            } 
            else {
                Finish ();
            }
        }
    }
}

